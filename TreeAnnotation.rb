#Treeannotator
#usage: ruby TreeAnnotation.rb <csv annotation file> <nodenumbered tree from AncestralStateReconstruction.R> <output tree name> <probability table from AncestralStateReconstruction.R>

ARGV
csv_annotation = ARGV[0]
input_tree = ARGV[1]
output_tree = ARGV[2]
output_table = ARGV[3]
ref = []
tes = []
node_ref = []
node_tes = []
test = false
reference = false
first_line = true

File.open(csv_annotation).each do |line|			#reads annotation
  if first_line
    first_line=false
    next
  elsif line.match(/,0/)
    ref << line.scan(/([A-Z]\w+)/).join
  elsif line.match(/,1/)
    tes << line.scan(/([A-Z]\w+)/).join
  end
end

#getting internal nodes
require 'csv'
table = CSV.read(output_table, headers: true)

(0...table.length).each do |x|
  if table[x]["0"].to_f>table[x]["1"].to_f
    node_ref << table[x][""]
  else
    node_tes << table[x][""]
  end
end

#creating another array with marked names
ref_marked = ref.map {|name| name + "{Reference}"}
tes_marked = tes.map {|name| name + "{Test}"}
node_ref_marked = node_ref.map {|name| name + "{Reference}"}
node_tes_marked = node_tes.map {|name| name + "{Test}"}
#combining the corresponding arrays into one
spec_name = ref + tes
spec_marked = ref_marked + tes_marked
node_name = node_ref + node_tes
node_name_marked = node_ref_marked + node_tes_marked

edited_text=""
File.open(input_tree).each do |line|
	(0...spec_name.length).each do |num|
		edited_text = line.gsub!("#{spec_name[num]}","#{spec_marked[num]}") || line
	end
 
	(0...node_name.length).each do |num|
		edited_text = line.gsub!(/\)#{node_name[num]}:/, "\)#{node_name_marked[num]}:") || line
	end
end

File.open(output_tree, "w") do |file|
  file.puts edited_text.gsub(";", "")   #also removes ; at the end of newick file, otherwise HyPhy cannot read this file
end