#fixes labels given by codonW, since codonW only gives the first 20 character as labels

ARGV
output_codonw=ARGV[0] 	
k_value = ARGV[1]		
output_coa=ARGV[2] 		

require 'csv'
table = CSV.read(k_value, headers: true)
species_name=table["species"]
key = table["species"].map {|names| if names.length>20; names.slice(0..19) else names end}
label_fix=[key, species_name].transpose.to_h

edited_texts=""
first_line=true
File.open(output_coa, "w") do |file|
	File.open(output_codonw).each do |line|
		found_key=line.scan(/_(\w{1,20})/).join
		if first_line
			first_line=false
			edited_texts=line.gsub("Axis2,", "Axis2")
		else
			edited_texts = line.gsub(line.scan(/(\d+_\w+),/).join, label_fix[found_key])
		end
		file.puts edited_texts
	end
end