#converts fasta to csv format for annotating tree branch as reference or test
#usage: ruby fasta-to-csv.rb < "fasta_file" > "csv_file_name" 

first_line = true
print "species,type"  #column names

while line = STDIN.gets
  line.chomp!

  if first_line==true
	first_line=false
	else
    puts unless first_line
    print line[0..-1].gsub(/\t/,",")
  end

  first_line = false
end

puts
