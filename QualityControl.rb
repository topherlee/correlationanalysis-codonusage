#replace frameshifts and stop codons with gaps
#run after using macse
require 'bio'
ARGV
input_file = ARGV[0]
output_file = ARGV[1]
new_seqs = []

File.open(output_file, "w") do |line|
  origin = Bio::FastaFormat.open(input_file)
  
  origin.each do |entry|
    seqs = Bio::Sequence::NA.new(entry.seq)
    seqs = seqs.to_s.split(/(.{3})/).reject {|element| element.empty?}  #split into triplets and cleanup empty elements
    seqs = seqs.map {|element| element.gsub(/(TAA|taa|TAG|tag|TGA|tga|UAA|uaa|UAG|uag|UGA|uga)/, "---")}.join #replaces stop codons with gaps
    new_seqs << Bio::Sequence::NA.new(seqs).gsub(/!/, "-").to_fasta(entry.definition) #replaces frameshift symbol '!' from macse with '-'
  end
  
  line.puts new_seqs
end