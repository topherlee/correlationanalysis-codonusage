#extract K value from JSON
ARGV
jsonfile = ARGV[0]
outputfile = ARGV[1]
require 'json'
file = File.open(jsonfile)
data = JSON.load file
file.close

#stores key species names
species = data["branch attributes"]["0"].keys.reject {|names| names.match(/Node/)}	#rejects internal node values

#outputs
delimiter = ","
File.open(outputfile, "w") do |files|
  files.puts "species,k"				#writes the header of the csv file
  species.each do |y|
	files.puts "#{y}" + "#{delimiter}" + "#{data["branch attributes"]["0"]["#{y}"]["k (general descriptive)"]}"
  end
end
