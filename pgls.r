library(ape)
library(nlme)
library(geiger)
library(phytools)
args = commandArgs(trailingOnly=TRUE)

#PCA from codonW with 2 axis
coa<-read.csv(args[1],header = TRUE)

#K value from hyphy
k_value<-read.csv(args[2],header = TRUE)

#merge both csv files
table<-merge(k_value, coa, by.x='species', by.y='label')
#set row names of the table
row.names(table)<-table$species

tree<-read.tree(args[3])
plot(tree)

pglsModel <- gls(k ~ Axis1*Axis2, correlation = corBrownian(phy = tree),
                 data = table, method = "ML")

write.csv(anova(pglsModel),file = args[4])
