#converts fasta to csv format for annotating tree branch as reference or test
#usage: ruby fasta-to-csv.rb < "fasta_file" > "csv_file_name" 

first_line = true
puts "species,type"  #column names

while line = STDIN.gets
  line.chomp!

  if line =~ /^>/
    puts unless first_line
    print line[1..-1]
    print ","  # <-- Change this to "\t" and it's a convert-fasta-to-tab
  else
    print line
  end

  first_line = false
end

puts
