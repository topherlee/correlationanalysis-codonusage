#!/bin/bash
#This script was tested on Linux in Oracle VM and uses MACSE v2.03, HyPhy v2.5.0(MP), CodonW v1.4.4, Ruby v2.5.1p57, R v3.6.1
#requires the scripts QualityControl.rb, tab-to-csv.rb, AncestralStateReconstruction.R, TreeAnnotation.rb, JsonParser.rb, and CodonwOutputRepair.rb in the same folder as this script
#usage: ./CorrelationAnalysis.sh <path to macse.jar> <FULL path to input folder> <path to fasta formatted taxa annotation file> <path to complete species tree file>

#CodonW only takes the first 20 alphabets of the sequence names, it is therefore very important that the first 20 alphabets of the sequence names are unique
#when the species names in the sequence files are too long (>20 alphabets) it has to be manually edited (annotation file, sequences file, newick file)
#example: Corallorhiza_maculata_var_maculata & Corallorhiza_maculata_var_mexicana will be recognized by codonw as Corallorhiza_maculat & Corallorhiza_maculat
#It is recommended to edit the sequence names so that it only contains the unique species names
#example: >Corallorhiza_maculata_var_maculata into >MA_Corallorhiza_maculata_var_maculata and >Corallorhiza_maculata_var_mexicana into >ME_Corallorhiza_maculata_var_mexicana

#input file is a gene sequences pro file, headers has to only contain the species name, sequences are expected to not contain duplicates
#more detail:
macse_location=$1		#exact path of macse jar file, example: '/home/user/Desktop/macse_v2.03.jar'
seq_folder=$2			#***FULL*** path to the folder containing **only** the input sequence(s)
annotation_file=$3		#exact path to .tsv formatted taxa annotation file (0 as {Reference} and 1 as {Test})
species_tree=$4			#input species tree file in newick format with branch lengths
work_dir=$(pwd)
########################################################################################################################################

#######################################Create folders###############################################
mkdir "$seq_folder/output"																			#main output folder, .csv files are the final output files of the ANOVA analysis
mkdir "$seq_folder/output/raw_alignment"; macse_output=$seq_folder/output/raw_alignment				#folder containing unedited MACSE AA and NT alignments
mkdir "$seq_folder/output/cleaned_alignment"; alignments=$seq_folder/output/cleaned_alignment		#folder containing edited MACSE NT alignments for use with HyPhy
mkdir $seq_folder/output/misc_output; misc_output=$seq_folder/output/misc_output					#folder containing annotation file in csv format
csv_annotation=$misc_output/$(basename "$annotation_file" .fasta).csv								#csv formatted annotation file
mkdir "$seq_folder/output/pgls_tree"; pgls_tree=$seq_folder/output/pgls_tree						#folder containing node numbered but not yet labeled guide tree of every genes
mkdir $misc_output/probability_table																#trait probability table of every nodes of each gene
mkdir $seq_folder/output/guide_tree																	#labeled guide trees for HyPhy
mkdir "$seq_folder/output/hyphy_output/"; hyphy_output=$seq_folder/output/hyphy_output
mkdir "$hyphy_output/k_value"; k_value=$hyphy_output/k_value
mkdir "$seq_folder/output/codonw_output"; codonw_output=$seq_folder/output/codonw_output


##########################################Run macse alignment##############################################
for FILE in `ls $seq_folder/*.fasta`; do
	java -Xmx1024m -jar $macse_location -prog alignSequences -out_AA $macse_output/$(basename "$FILE" .fasta)_aa.fasta -out_NT $macse_output/$(basename "$FILE" .fasta)_nt.fasta -seq $FILE 
	#-Xmx1024m to limit memory usage of java
done

######################################Run QualityControl.rb################################################
for FILE in `ls $macse_output/*_nt.fasta`; do
	ruby QualityControl.rb $FILE $alignments/$(basename "$FILE" .fasta).fasta
done

#######################Converts fasta-formatted annotation file to csv for R###############################
#manually created annotation file
#probably not needed, can change the separator that will be read in R
ruby tab-to-csv.rb < $annotation_file > $csv_annotation


for FILE in `ls $macse_output/*_nt.fasta`; do
##################################Ancestral State Reconstruction############################################	
	probability_table=$misc_output/probability_table/fitER_$(basename "$FILE" _nt.fasta).csv				#table with trait probability of every internal nodes
	nodenumbered_tree=$pgls_tree/$(basename "$FILE" _nt.fasta).tre											#edited species tree with numbered internal nodes and dropped tip labels
	Rscript AncestralStateReconstruction.R $species_tree $FILE $csv_annotation $probability_table $nodenumbered_tree
	echo "Internal node trait probabilities stored in $probability_table/"

#######################Labeling reference and test to the whole branches and node###########################
	hyphy_tree=$seq_folder/output/guide_tree/$(basename "$FILE" _nt.fasta)_labeled.tre
	ruby TreeAnnotation.rb $csv_annotation $nodenumbered_tree $hyphy_tree $probability_table
	echo "A tree file with labeled branches and nodes (Reference or Test) is stored as $hyphy_tree"
done

#################################Run hyphy on all files in the selected folder##############################
job=1
echo "Running HyPhy RELAX, this might take a while"
for FILE in `ls $alignments/*.fasta`; do 
	echo "Running RELAX for file #$job : $(basename "$FILE" .fasta).fasta"
 	(echo "1"; echo "7"; echo $FILE; echo $seq_folder/output/guide_tree/$(basename "$FILE" _nt.fasta)_labeled.tre; echo "2"; echo "1") | hyphy CPU=32 > $hyphy_output/$(basename "$FILE" .fasta).log
 	#1:Selection Analysis
	#7:RELAX
	#echo $FILE: coding sequence alignment from MACSE cleaned alignment
	#echo $seq_folder/output/guide_tree/$(basename "$FILE" _nt.fasta)_labeled.tre: labeled guide tree for use with hyphy
	#echo "2": select **Test** as the _test_ set
	#echo "1": select **Reference** as the _reference set
	mv -f $alignments/$(basename "$FILE" .fasta).fasta.RELAX.json $hyphy_output/$(basename "$FILE" .fasta).json		#move JSON file created by hyphy to the corresponding hyphy output folder of each gene
	job=$( expr $job + 1 )
done

###################################Extracting K values of each branch######################################
for FILE in `ls $hyphy_output/*.json`; do
	echo "Parsing $(basename "$FILE")"
	ruby JsonParser.rb $FILE "$k_value/$(basename "$FILE" _nt.json).k.csv"
done

#################################################CodonW####################################################
#CodonW only takes the first 20 alphabets of the sequence names, it is therefore very important that the first 20 alphabets of the sequence names are unique
#when the species names in the sequence files are too long (>20 alphabets) it has to be manually edited (annotation file, sequences file, newick file)
#example: Corallorhiza_maculata_var_maculata & Corallorhiza_maculata_var_mexicana will be recognized by codonw as Corallorhiza_maculat & Corallorhiza_maculat
#will return warnings about sequences not ending with stop codons. This is normal because the MACSE alignments were first edited using QualityControl.rb and then CodonwControl.rb
cp $alignments/*_nt.fasta $codonw_output/
for FILE in `ls $codonw_output/*_nt.fasta`; do
	mkdir "$codonw_output/$(basename "$FILE" _nt.fasta)"
	ruby CodonwControl.rb $FILE $codonw_output/$(basename "$FILE" _nt.fasta)/$(basename "$FILE")		#edits the MACSE cleaned alignment files for use with CodonW. Since CodonW won't recognize gaps '-', they will be replaced with 'x'
	cd $codonw_output
	gene_names=$(basename "$FILE" _nt.fasta)
	cd $gene_names			#move to the respective folder so that the main folder is uncluttered and the results are not overwritten
	codonw $(basename "$FILE") -coa_cu -silent -nomenu -t, -coa_axes 2 #-coa_axes determines the principal components that will be written in output files
	mv genes.coa $codonw_output/$(basename "$FILE" _nt.fasta).codonw.csv
	cd $codonw_output
	rm $(basename "$FILE")
	cd $work_dir
done
echo "Correspondence analysis of codon usage done, files are stored in $codonw_output"

#####################################Fix CodonW output labels#############################################
#This step will fix the sequence names in the output file that were cut off by codonW
for CODON_CSV in `ls $codonw_output/*.codonw.csv`; do 
		echo "Fixing output file $(basename "$CODON_CSV" .codonw.csv)"
		ruby CodonwOutputRepair.rb $CODON_CSV $k_value/$(basename "$CODON_CSV" .codonw.csv).k.csv $codonw_output/$(basename "$CODON_CSV" .codonw.csv).coa.csv
		rm $CODON_CSV		#remove old codonw output
done

######################################PGLS and ANOVA analysis#############################################
for FILE in `ls $k_value/*.k.csv`; do
	Rscript pgls.r $codonw_output/$(basename "$FILE" .k.csv).coa.csv $FILE $pgls_tree/$(basename "$FILE" .k.csv).tre $seq_folder/output/$(basename "$FILE" .k.csv).csv
done

echo -e "K values are saved to $hyphy_output/k_value\n"
echo -e "Correspondence analysis of codon usage done, files are stored in $codonw_output\n"
echo -e "ANOVA analysis results are stored in $seq_folder/output/"